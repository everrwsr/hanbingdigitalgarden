---
{"dg-publish":true,"permalink":"/02dev/激活windows教程和更换windows版本教程/","dgPassFrontmatter":true}
---

#技能 

1的好处是不额外下软件，
2的好处是简单万能有图像界面

我推荐2

[[00inbox/jianyue/webpage/Windows_Office 永久激活工具 - 不死鸟 - 分享为王官网\|Windows_Office 永久激活工具 - 不死鸟 - 分享为王官网]]




1.microsoft active script 

使用powershell 然后粘贴这个
```
irm https://massgrave.dev/get | iex
```


![image.png](https://cdn.jsdelivr.net/gh/everrwsr/blogimage@master/202401162244665.png)

官方下载地址
[GitHub - massgravel/Microsoft-Activation-Scripts: A Windows and Office activator using HWID / Ohook / KMS38 / Online KMS activation methods, with a focus on open-source code and fewer antivirus detections.](https://github.com/massgravel/Microsoft-Activation-Scripts)

点击 extra 可以更换windows版本


使用教程

[Win10 激活-微软激活脚本（MAS） - 阿坦 - 博客园](https://www.cnblogs.com/lizhiqiang0204/p/16993840.html)



2.heu kms activite

官方下载地址
[Releases · zbezj/HEU\_KMS\_Activator](https://github.com/zbezj/HEU_KMS_Activator/releases)

我给你整的地址
[文件](https://wwjp.lanzoul.com/i7cZn1h2d1yj?password=1bqq)

使用教程
![image.png](https://cdn.jsdelivr.net/gh/everrwsr/blogimage@master/202312071145457.png)


等进度条走到100%
![image.png](https://cdn.jsdelivr.net/gh/everrwsr/blogimage@master/202312071146106.png)


成功提示

![image.png](https://cdn.jsdelivr.net/gh/everrwsr/blogimage@master/202312071146098.png)


好了


更改windows版本和通道
按照123依次来，会提醒你重启，记得重启以后，再次打开这个软件，按照上面的激活教程激活下
一般家庭版是微软小白鼠
更新多，不稳定
专业版好点


![image.png](https://cdn.jsdelivr.net/gh/everrwsr/blogimage@master/202312071151170.png)


[Windows/Office永久激活工具 - 不死鸟 - 分享为王官网](https://iui.su/2562/)

